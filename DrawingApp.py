import numpy as np


def get_commands(filename):
    try:
        with open(filename, 'r') as file:
            text = file.read()
        commands = text.split('\n')
        return commands
    except FileNotFoundError:
        raise Exception('File \'{0}\' not found'.format(filename))


def create_canvas(width, height):
    if width <= 0 or height <= 0:
        return False
    canvas = np.zeros(shape=(height, width))
    return canvas


def create_line(canvas, x1, y1, x2, y2):
    if x1 == x2:
        if y2 < y1:
            temp = y2
            y2 = y1
            y1 = temp
        for i in range(y1-1, y2):
            canvas[i][x1-1] = 1
    elif y1 == y2:
        if x2 < x1:
            temp = x2
            x2 = x1
            x1 = temp
        for i in range(x1-1, x2):
            canvas[y1-1][i] = 1
    else:
        print('Line not created')
    return canvas


def create_rectangle(canvas, x1, y1, x2, y2):
    canvas = create_line(canvas, x1, y1, x2, y1)
    canvas = create_line(canvas, x1, y1, x1, y2)
    canvas = create_line(canvas, x1, y2, x2, y2)
    canvas = create_line(canvas, x2, y1, x2, y2)
    return canvas


def make_fill(canvas, size, x, y, num):
    if x >= int(size[0]) or x < 0 or y >= int(size[1]) or y < 0:
        return canvas
    if canvas[y][x] == 2:
        return canvas
    if canvas[y][x] != num:
        canvas[y][x] = 2
        canvas = make_fill(canvas, size, x-1, y, num)
        canvas = make_fill(canvas, size, x, y-1, num)
        canvas = make_fill(canvas, size, x+1, y, num)
        canvas = make_fill(canvas, size, x, y+1, num)
    return canvas


def fill(canvas, size, x, y):
    if canvas[y-1][x-1] == 0:
        canvas = make_fill(canvas, size, x-1, y-1, 1)
    elif canvas[y-1][x-1] == 1:
        canvas = make_fill(canvas, size, x-1, y-1, 0)
    return canvas


def print_canvas(canvas, symbol=' '):
    size = canvas.shape
    start_end_line = ''.join('-' for i in range(size[1]+2))
    result = start_end_line + '\n'
    for i in range(0, size[0]):
        line = '|'
        for num in range(0, size[1]):
            if canvas[i][num] == 1:
                line += 'x'
            elif canvas[i][num] == 2:
                line += symbol
            elif canvas[i][num] == 0.:
                line += ' '
        result += line
        result += '|\n'
    result += start_end_line + '\n'
    return result


def make_output(result, filename):
    with open(filename, 'w') as file:
        file.write(result)


def draw():
    try:
        commands = get_commands('input.txt')
    except Exception as e:
        make_output(str(e), 'output.txt')
        return
    canvas = False
    symbol = ' '
    for command in commands:
        if command[0] == 'C':
            words = command.split(' ')
            size = words[1:]
            canvas = create_canvas(int(size[0]), int(size[1]))
            if canvas is False:
                result = 'Canvas not created'
                break
            result = print_canvas(canvas, symbol)
        if command[0] == 'L':
            if canvas is False:
                result = 'Canvas not created'
                break
            words = command.split(' ')
            canvas = create_line(canvas, int(words[1]), int(
                words[2]), int(words[3]), int(words[4]))
            result += print_canvas(canvas, symbol)
        elif command[0] == 'R':
            if canvas is False:
                result = 'Canvas not created'
                break
            words = command.split(' ')
            canvas = create_rectangle(canvas, int(words[1]), int(
                words[2]), int(words[3]), int(words[4]))
            result += print_canvas(canvas, symbol)
        elif command[0] == 'B':
            if canvas is False:
                result = 'Canvas not created'
                break
            words = command.split(' ')
            canvas = fill(canvas, size, int(words[1]), int(words[2]))
            symbol = words[3]
            result += print_canvas(canvas, symbol)
    make_output(result, 'output.txt')


if __name__ == "__main__":
    draw()
