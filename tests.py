import unittest
import numpy as np
import DrawingApp as da


class MyTest(unittest.TestCase):

    def test_file_open(self):
        self.assertEqual(da.get_commands('testfile.txt'), ['1', '2', '3', '4'])

    def test_create_canvas(self):
        canvas = np.zeros(shape=(3, 3))
        self.assertEqual(da.create_canvas(3, 3).all(), canvas.all())

    def test_create_line(self):
        canvas = np.zeros(shape=(3, 3))
        finish_canvas = np.array([[0, 0, 0],
                                  [0, 0, 0],
                                  [1, 1, 1]])
        self.assertEqual(da.create_line(
                         canvas, 1, 3, 3, 3).all(), finish_canvas.all())

    def test_create_rectangle(self):
        canvas = np.array([[0, 0, 0],
                           [0, 0, 0],
                           [0, 0, 0]])
        finish_canvas = np.array([[1, 1, 1],
                                  [1, 0, 1],
                                  [1, 1, 1]])
        self.assertEqual(da.create_rectangle(
                         canvas, 1, 3, 3, 3).all(), finish_canvas.all())

    def test_make_fill(self):
        canvas = np.array([[0, 0, 1, 0, 1, 1, 1, 0],
                           [0, 0, 1, 0, 1, 0, 1, 0],
                           [1, 1, 1, 0, 1, 1, 1, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0],
                           [1, 0, 1, 1, 1, 1, 1, 1],
                           [1, 0, 0, 0, 0, 0, 0, 0]])
        finish_canvas = np.array([[0, 0, 1, 2, 1, 1, 1, 2],
                                  [0, 0, 1, 2, 1, 0, 1, 2],
                                  [1, 1, 1, 2, 1, 1, 1, 2],
                                  [2, 2, 2, 2, 2, 2, 2, 2],
                                  [2, 2, 2, 2, 2, 2, 2, 2],
                                  [2, 2, 2, 2, 2, 2, 2, 2],
                                  [1, 2, 1, 1, 1, 1, 1, 1],
                                  [1, 2, 2, 2, 2, 2, 2, 2]])
        self.assertEqual(da.make_fill(
                         canvas, [8, 8], 4, 5, 0).all(), finish_canvas.all())

    def test_print_canvas(self):
        canvas = np.array([[0, 0, 1, 2, 1, 1, 1, 2],
                           [0, 0, 1, 2, 1, 0, 1, 2],
                           [1, 1, 1, 2, 1, 1, 1, 2],
                           [2, 2, 2, 2, 2, 2, 2, 2],
                           [2, 2, 2, 2, 2, 2, 2, 2],
                           [2, 2, 2, 2, 2, 2, 2, 2],
                           [1, 2, 1, 1, 1, 1, 1, 1],
                           [1, 2, 2, 2, 2, 2, 2, 2]])
        result = '----------\n|  xYxxxY|\n|  xYx xY|\n|xxxYxxxY|\n|YYYYYYYY|\n|YYYYYYYY|\n|YYYYYYYY|\n|xYxxxxxx|\n|xYYYYYYY|\n----------\n'
        self.assertEqual(da.print_canvas(
                         canvas, 'Y'), result)


if __name__ == '__main__':
    unittest.main()
